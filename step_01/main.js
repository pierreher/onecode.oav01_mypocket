const http = require('http');
const hostname = 'localhost';
const port = parseInt(process.argv[2]||4242);

http.createServer((req,res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');


    
}).listen(port,hostname,()=>{
  console.log(`Server running at http://${hostname}:${port}`);
});
