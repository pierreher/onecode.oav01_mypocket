const express = require('express');
const app = express();

console.log("Registering endpoint: /");
app.get('/', (req,res)=>{
    res.send('hello ROOT world\n');
});

console.log("Registering endpoint: /stubbed");
app.get('/stubbed', (req,res)=>{
    res.send('hello STUBBED\n');
});

console.log("Registering endpoint: /testing");
app.get('/testing', (req,res)=>{
    res.send('this is a test endpoint\n');
});

console.log("Registering endpoint: /jsonendpoint");
app.get('/jsonendpoint', (req,res)=>{
    res.json({
        "mykey" : "myvalue",
        "testy" : "something",
        "exnum" : 123
    });
});

app.listen(3000);
