const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('data/oavdb01');


  function setUser() {
    const data = ['UserLambda','email@lambda.fr']
    db.run("INSERT INTO users (nickname,email) VALUES (?,?)",data,(err)=>{
      if(err) { throw err; }
      return 'User created.\n';
    });
  }

  function getAllUsers(response) {
    let output='';
    db.all("SELECT * FROM users",(err,rows) => {
      output = JSON.parse(JSON.stringify(rows));
      response.json(output);
    });
  }

  function getUser(id) {
    db.get("SELECT * FROM users WHERE id = ?",id, (err,row)=>{
      if (row!==undefined) {
        let output = `{
      id : ${row.id},
      nickname : ${row.nickname}
  }
    `;
        return output;
      } else {
          return 'User not found !\n';
      }

    });
  }

  function update(id) {
    const data = ['Maurice',id]
    const update = `UPDATE users
                  SET nickname = ?
                  WHERE id = ?`;
    db.run(update, data, (err, row)=>{
        if(err) { throw err; }
        return `User ${id} is now called ${data[0]}.`;
    });
  }

  function deleteOne(id) {
    db.run(`DELETE FROM users WHERE id=?`,id,(err)=>{
      if(err) { throw err; res.end(); }
      return 'Row deleted.\n';
    })
  }



module.exports = { setUser, getAllUsers, getUser, update, deleteOne };
