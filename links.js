const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('data/oavdb01');
const express = require('express');
const api = express();

/**************************************/
/******     --- CRUD ---       ********/
/**************************************/

api.get('/', (req,res)=>{
    res.send('ROOT LEVEL\n');
});

/****** CREATE ********/

api.post('/users/:userId/links/create',(req,res)=>{
  const data = ['Facebook','facebook.com/userlambda',req.params.userId]
  db.run("INSERT INTO links (tags,url,user_id) VALUES (?,?,?)",data,(err)=>{
    if(err) { throw err; res.end(); }
    res.send('Link created.\n');
    res.end();
  });
});

/****** READ ********/

// ALL
api.get('/users/:userId/links', (req,res)=>{
  let output = '';
  res.set('Content-Type', 'application/json');
  db.all("SELECT * FROM links WHERE user_id = ?",userId,(err,rows)=>{
    if(err) { throw err; res.end(); }
    rows.forEach(row=>{
      output += `{
  id : ${row.id},
  user_id : ${row.user_id},
  tag : ${row.tags}
}
`;
    })
    res.send(output);
  });
});


// ONE
api.get('/users/:userId/links/:id', (req,res)=> {
  res.set('Content-Type', 'application/json');
  db.get("SELECT * FROM users WHERE user_id = ? AND id = ?",req.params.user_id,req.params.id, (err,row)=>{
    if (row!==undefined) {
      res.send(`{
    id : ${row.id},
    user_id : ${row.user_id},
    tag : ${row.tags}
}
  `);
    } else {
      res.send('Link not found !\n')
    }

  });

});

/****** UPDATE ********/

api.post('users/:userId/links/:id', (req,res)=>{
  const data = ['Cuisine',req.params.id]
  const update = `UPDATE links
                SET tags = ?
                WHERE id = ?`;
  db.run(update, data, (err, row)=>{
      if(err) { throw err; res.end(); }
      else { res.status(202); }
      res.send(`Link ${req.params.id} has now ${data[0]} tag.
`);
      res.end();
  });
});


/****** DELETE ********/
api.post('/users/:userId/links/:id/delete',(req,res)=>{
  db.run(`DELETE FROM links WHERE id=?`,req.params.id,(err)=>{
    if(err) { throw err; res.end(); }
    res.send('Link deleted.\n');
    res.end();
  })
})


api.listen(3000);
