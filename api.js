const http = require('http');
const hostname = 'localhost';
const port = parseInt(process.argv[2]||3000);
const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('data/oavdb01');
const express = require('express');
const api = express();

const users = require('./users.js');
const links = require('./links.js');

db.serialize(()=>{
  db.run(`CREATE TABLE IF NOT EXISTS users (
      id INTEGER PRIMARY KEY,
      nickname TEXT,
      email TEXT,
      password TEXT
  )`);

  db.run(`INSERT INTO users(nickname,email,password)
          VALUES (?,?,?)`,'Jack','jack@jack.fr','ojack');

  db.run(`CREATE TABLE IF NOT EXISTS links (
      id INTEGER PRIMARY KEY,
      tags TEXT,
      url TEXT,
      user_id INTEGER NOT NULL,
      FOREIGN KEY (user_id) REFERENCES users(id)
  )`);

  db.run(`INSERT INTO links(tags,url,user_id)
          VALUES (?,?,?)`,'Cuisine',2,1);

});

  /*******************************************/
  /******     --- CRUD USER ---       ********/
  /*******************************************/

  api.get('/', (req,res)=>{
      res.send('ROOT LEVEL\n');
  });

  /****** CREATE ********/
  api.post('/users/create',(req,res)=> users.create());

  /****** READ ********/
  // ALL

  api.get('/users', (req,res)=> {
    (users.getAllUsers(res))
  });

  // ONE
  api.get('/users/:id', (req,res)=> users.getUser(req.params.id));

  /****** UPDATE ********/
  api.post('/users/:id', (req,res)=> users.update(req.params.id));

  /****** DELETE ********/
  api.post('/users/:id/delete',(req,res)=> users.deleteOne(req.params.id));

  api.listen(port);
  console.log(`Server running at http://${hostname}:${port}`);
